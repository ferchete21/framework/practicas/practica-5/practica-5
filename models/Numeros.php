<?php
namespace app\models;

use yii\base\Model;

class Numeros extends Model{
    public $numero1;
    public $numero2;
    public $tipo;
    public $sumar;
    public $restar;
    public $multiplicar;
    public $dividir;
    //El attribute labels si tu creas un formulario de este modelo
    //te crea las etiquetas de labels
    public function attributeLabels() {
        return[
          "numero1" => "El número 1",
          "numero2"=>"El número 2",
          "sumar"=>"La suma es",
          "restar"=>"La resta es",
          "multiplicar"=>"La multiplicación es",
          "dividir"=>"La división es",
            
            
        ];
    
    }  
    public function rules(){
        return[
            [['numero1','numero2'],'integer'],
            [['numero1','numero2'],'required'],
//            ['numero2', 'compare', 'compareValue' => 0, 'operator' => '!=', 'type' => 'number','when'=>function($model){
//            $model->tipo=="dividir"; //Solo realiza la validacion si la operacion es dividir
//            }]
            ['numero2','dividirXCero']

        ];
    }  
    //Esta regla me comprueba que no estemos dividiendo por cero
    public function dividirXCero($atributo,$parametros) {
    if($this->tipo=="dividir"){
        if($this->numero2==0){
            $this->addError("numero2","PERO COMO VAS A PONER UN 0, QUE ESTAS DIVIENDO BURRO");
            }
        }
    }
        
      public function operacion() {
          
          switch($this->tipo){
              case 'sumar':
                  return $this->numero1+$this->numero2;
                  break;
              case 'restar':
                  return $this->numero1-$this->numero2;
                  break;
              case 'multiplicar':
                  return $this->numero1*$this->numero2;
                  break;
              case 'dividir':
                  return $this->numero1/$this->numero2;
                  break;
              return 0;
          }
      } 
          

          public function operarTodo() {
//              $this->tipo="sumar";
//              $this->sumar= $this->operacion();
//              
//              $this->tipo="restar";
//              $this->sumar= $this->operacion();
            $this->sumar=$this->numero1+$this->numero2;
              $this->restar=$this->numero1-$this->numero2;
              $this->multiplicar=$this->numero1*$this->numero2;
              $this->dividir=$this->numero1/$this->numero2;
              
              
          }
      }  
   
    

