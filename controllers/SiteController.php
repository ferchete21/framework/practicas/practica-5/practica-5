<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;
use app\models\Numeros;

class SiteController extends Controller
{
    /**
     * {@inheritdoc}
     */
  

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
        return $this->render('index');
    }
    
    public function actionSumar()
    {
        $model= new Numeros();//creando un modelo vacio que crea una clase que me crea los campos
        $model->tipo="sumar";
        $datos= Yii::$app->request->post();//recoger lo que me manda el formulario
        
        if($model->load($datos) && $model->validate()){//Compruebo si me llega algo del formulario y si siguen las rules
            //realizo la suma
            $resultado=$model->operacion("sumar");
            //La mando a la vista
            return $this->render("resultado",[
                "resultado" => $resultado,
                    "operacion"=>"Suma"
            ]) ;
        }
        return $this->render('numeros', [
        'model' => $model,
            'operacion' => 'Sumar'
        
    ]);
    }

    public function actionRestar()
    {
        $model= new Numeros();   //creando un modelo vacio que crea una clase que me crea los campos
        $model->tipo="restar";
        $datos= Yii::$app->request->post();//recoger lo que me manda el formulario
        
        if($model->load($datos) && $model->validate()){//Compruebo si me llega algo del formulario y si siguen las rules
            //realizo la resta
            $resultado=$model->operacion("restar");;
            //La mando a la vista
            return $this->render("resultado",[
                "resultado" => $resultado,
                "operacion"=>"Restar"
            ]) ;
        }
        return $this->render('numeros', [
        'model' => $model,
            'operacion' => 'restar'
        
    ]);
    }
   
    public function actionMultiplicar()
    {
        $model= new Numeros();   //creando un modelo vacio que crea una clase que me crea los campos
        $model->tipo="multiplicar";
        $datos= Yii::$app->request->post();//recoger lo que me manda el formulario
        
        if($model->load($datos) && $model->validate()){//Compruebo si me llega algo del formulario y si siguen las rules
            //realizo la resta
            $resultado=$model->operacion("multiplicar");
            //La mando a la vista
            return $this->render("resultado",[
                "resultado" => $resultado,
                "operacion"=>"Multiplicar"
            ]) ;
        }
        return $this->render('numeros', [
        'model' => $model,
            'operacion' => 'multiplicar'
        
    ]);
    
    }
    
    public function actionDividir()
    {
        $model= new Numeros();   //creando un modelo vacio que crea una clase que me crea los campos
        $model->tipo="dividir" ||$this->tipo=="todo";
        $datos= Yii::$app->request->post();//recoger lo que me manda el formulario
        
        if($model->load($datos) && $model->validate()){//Compruebo si me llega algo del formulario y si siguen las rules
            //realizo la resta
            $resultado=$model->operacion("dividir");
            //La mando a la vista
            return $this->render("resultado",[
                "resultado" => $resultado,
                "operacion"=>"Dividir"
            ]) ;
        }
        return $this->render('numeros', [
        'model' => $model,
            "operacion"=>"Dividir"
        
    ]);
    
    }
    
      public function actionOperar(){
        $model= new Numeros();   //creando un modelo vacio que crea una clase que me crea los campos
        $model->tipo="todo";
        $datos= Yii::$app->request->post();//recoger lo que me manda el formulario
        
        if($model->load($datos) && $model->validate()){//Compruebo si me llega algo del formulario y si siguen las rules
            //realizo todas las operaciones
            $model->operarTodo();
            //La mando a la vista
            return $this->render("mostrarTodo",[
                "model" => $model
            ]) ;
        }
        return $this->render('numeros', [
        'model' => $model,
        "operacion"=>"Calcular"
    ]);
    }

   
}
