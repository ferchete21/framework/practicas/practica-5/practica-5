<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Sumar */
/* @var $form ActiveForm */
?>
<div class="site-sumar">

    <?php $form = ActiveForm::begin(); ?>

        <?= $form->field($model, 'numero1') ?>
        <?= $form->field($model, 'numero2') ?>
    
        <div class="form-group">
            <?= Html::submitButton($operacion, ['class' => 'btn btn-primary']) ?>
        </div>
    <?php ActiveForm::end(); ?>

</div><!-- site-sumar -->
